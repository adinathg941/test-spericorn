<?php

return [
    'email_verify_url' => env('VUE_URL', 'http://localhost/verify-email/')
];
